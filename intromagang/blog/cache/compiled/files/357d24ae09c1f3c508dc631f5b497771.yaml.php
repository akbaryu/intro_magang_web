<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'themes://purity/purity.yaml',
    'modified' => 1469684843,
    'data' => [
        'enabled' => true,
        'tagline' => 'Creating a blog layout using Pure',
        'footer' => 1,
        'layout' => 0,
        'style' => 0,
        'menu' => 0
    ]
];
