<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'plugins://error/error.yaml',
    'modified' => 1469691410,
    'data' => [
        'enabled' => true,
        'routes' => [
            404 => '/error'
        ]
    ]
];
