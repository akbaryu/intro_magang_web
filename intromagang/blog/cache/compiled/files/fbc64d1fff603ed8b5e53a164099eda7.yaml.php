<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/grav/user/accounts/akbaryu.yaml',
    'modified' => 1469519403,
    'data' => [
        'email' => 'akbaryu@gmail.com',
        'fullname' => 'Yudha Akbar',
        'title' => 'Admin',
        'state' => 'enabled',
        'access' => [
            'admin' => [
                'login' => true,
                'super' => true
            ],
            'site' => [
                'login' => true
            ]
        ],
        'hashed_password' => '$2y$10$pJfXMtO74r7akvB6N7AyeuIZnltiv9Bls6f3f9VJYZZjkaNNjwdgK'
    ]
];
