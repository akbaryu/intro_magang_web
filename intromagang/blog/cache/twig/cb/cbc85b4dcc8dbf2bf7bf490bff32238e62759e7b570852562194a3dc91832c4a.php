<?php

/* forms/fields/spacer/spacer.html.twig */
class __TwigTemplate_e19146f5b898e2256bd4c698869e78de815b8c2b4dad09bf76f2acb93d98603f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"form-spacer\">
    ";
        // line 2
        if ($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "title", array())) {
            // line 3
            echo "        <h3>
            ";
            // line 4
            if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["grav"]) ? $context["grav"] : null), "twig", array(), "any", false, true), "twig", array(), "any", false, true), "filters", array(), "any", false, true), "tu", array(), "array", true, true)) {
                // line 5
                echo $this->env->getExtension('AdminTwigExtension')->tuFilter($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "title", array()));
            } else {
                // line 7
                echo $this->env->getExtension('GravTwigExtension')->translate($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "title", array()));
            }
            // line 9
            echo "        </h3>
    ";
        }
        // line 11
        echo "
    ";
        // line 12
        if ($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "markdown", array())) {
            // line 13
            echo "        <p>
            ";
            // line 14
            if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["grav"]) ? $context["grav"] : null), "twig", array(), "any", false, true), "twig", array(), "any", false, true), "filters", array(), "any", false, true), "tu", array(), "array", true, true)) {
                // line 15
                echo $this->env->getExtension('GravTwigExtension')->markdownFilter($this->env->getExtension('AdminTwigExtension')->tuFilter($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "text", array())));
            } else {
                // line 17
                echo $this->env->getExtension('GravTwigExtension')->markdownFilter($this->env->getExtension('GravTwigExtension')->translate($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "text", array())));
            }
            // line 19
            echo "        </p>
    ";
        } else {
            // line 21
            echo "        <p>
            ";
            // line 22
            if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["grav"]) ? $context["grav"] : null), "twig", array(), "any", false, true), "twig", array(), "any", false, true), "filters", array(), "any", false, true), "tu", array(), "array", true, true)) {
                // line 23
                echo $this->env->getExtension('AdminTwigExtension')->tuFilter($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "text", array()));
            } else {
                // line 25
                echo $this->env->getExtension('GravTwigExtension')->translate($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "text", array()));
            }
            // line 27
            echo "        </p>
    ";
        }
        // line 29
        echo "
    ";
        // line 30
        if ($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "underline", array())) {
            // line 31
            echo "    <hr />
    ";
        }
        // line 33
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "forms/fields/spacer/spacer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 33,  79 => 31,  77 => 30,  74 => 29,  70 => 27,  67 => 25,  64 => 23,  62 => 22,  59 => 21,  55 => 19,  52 => 17,  49 => 15,  47 => 14,  44 => 13,  42 => 12,  39 => 11,  35 => 9,  32 => 7,  29 => 5,  27 => 4,  24 => 3,  22 => 2,  19 => 1,);
    }
}
/* <div class="form-spacer">*/
/*     {% if field.title %}*/
/*         <h3>*/
/*             {% if grav.twig.twig.filters['tu'] is defined %}*/
/*                 {{- field.title|tu|raw -}}*/
/*             {% else %}*/
/*                 {{- field.title|t|raw -}}*/
/*             {% endif %}*/
/*         </h3>*/
/*     {% endif %}*/
/* */
/*     {% if field.markdown %}*/
/*         <p>*/
/*             {% if grav.twig.twig.filters['tu'] is defined %}*/
/*                 {{- field.text|tu|markdown|raw -}}*/
/*             {% else %}*/
/*                 {{- field.text|t|markdown|raw -}}*/
/*             {% endif %}*/
/*         </p>*/
/*     {% else %}*/
/*         <p>*/
/*             {% if grav.twig.twig.filters['tu'] is defined %}*/
/*                 {{- field.text|tu|raw -}}*/
/*             {% else %}*/
/*                 {{- field.text|t|raw -}}*/
/*             {% endif %}*/
/*         </p>*/
/*     {% endif %}*/
/* */
/*     {% if field.underline %}*/
/*     <hr />*/
/*     {% endif %}*/
/* </div>*/
/* */
