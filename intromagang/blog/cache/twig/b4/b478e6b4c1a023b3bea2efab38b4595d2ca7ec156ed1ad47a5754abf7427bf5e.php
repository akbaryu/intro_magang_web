<?php

/* partials/blog_sidebar_footer.html.twig */
class __TwigTemplate_86d68a30503765dcc2225102c07bd13de868db680861455d6b1dca00e8d7b36c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "                        </div>
                                </div>
                            </div>
                            <div class=\"clear\"></div>
                        </div>
                    </div>
                    <div class=\"clear\"></div>
                </div>
            </div>
            <div class=\"clear\"></div>
        </div>
    </div>
</div>
<div class=\"clear\"></div>
</div>

<div class=\"gdlr-sidebar gdlr-right-sidebar four1 columns\">
<div class=\"gdlr-item-start-content sidebar-right-item\">
    <div class=\"section\" id=\"swid\">
        ";
        // line 20
        $this->loadTemplate("partials/sidebar.html.twig", "partials/blog_sidebar_footer.html.twig", 20)->display($context);
        // line 21
        echo "    </div>
</div>
</div>
<div class=\"clear\"></div>
</div>
</div>";
    }

    public function getTemplateName()
    {
        return "partials/blog_sidebar_footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 21,  40 => 20,  19 => 1,);
    }
}
/*                         </div>*/
/*                                 </div>*/
/*                             </div>*/
/*                             <div class="clear"></div>*/
/*                         </div>*/
/*                     </div>*/
/*                     <div class="clear"></div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="clear"></div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* <div class="clear"></div>*/
/* </div>*/
/* */
/* <div class="gdlr-sidebar gdlr-right-sidebar four1 columns">*/
/* <div class="gdlr-item-start-content sidebar-right-item">*/
/*     <div class="section" id="swid">*/
/*         {% include 'partials/sidebar.html.twig' %}*/
/*     </div>*/
/* </div>*/
/* </div>*/
/* <div class="clear"></div>*/
/* </div>*/
/* </div>*/
