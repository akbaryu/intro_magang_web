<?php

/* forms/fields/file/file.html.twig */
class __TwigTemplate_eb12d4158f83ef5708079b2d5d43ac241a74091812c4186534fee27b6c3faaae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("forms/field.html.twig", "forms/fields/file/file.html.twig", 1);
        $this->blocks = array(
            'input' => array($this, 'block_input'),
            'input_attributes' => array($this, 'block_input_attributes'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "forms/field.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["defaults"] = $this->getAttribute($this->getAttribute((isset($context["config"]) ? $context["config"] : null), "plugins", array()), "form", array());
        // line 3
        $context["files"] = twig_array_merge($this->getAttribute((isset($context["defaults"]) ? $context["defaults"] : null), "files", array()), ((array_key_exists("field", $context)) ? (_twig_default_filter((isset($context["field"]) ? $context["field"] : null), array())) : (array())));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 39
    public function block_input($context, array $blocks = array())
    {
        // line 40
        echo "    ";
        $context["page_can_upload"] = ((isset($context["exists"]) ? $context["exists"] : null) || ((((isset($context["type"]) ? $context["type"] : null) == "page") &&  !(isset($context["exists"]) ? $context["exists"] : null)) &&  !((is_string($__internal_4f808d1b949e3b8641293d677ae6c5d3560b298a9b0f6a1a11c3a278228346fc = $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "destination", array())) && is_string($__internal_27a5108610a15a53edb5efcf5630189e75d5d09883fc4fcd4c0e1d1124a37b8d = "@self") && ('' === $__internal_27a5108610a15a53edb5efcf5630189e75d5d09883fc4fcd4c0e1d1124a37b8d || 0 === strpos($__internal_4f808d1b949e3b8641293d677ae6c5d3560b298a9b0f6a1a11c3a278228346fc, $__internal_27a5108610a15a53edb5efcf5630189e75d5d09883fc4fcd4c0e1d1124a37b8d))) || (is_string($__internal_751e51172e9129657f1f0e84cfe46a6747d912265b8b8dcab59f735f84194238 = $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "destination", array())) && is_string($__internal_fbae7afec264bc831641ca6a413b442c5245396641f126e3ae317de45e0a9909 = "self@") && ('' === $__internal_fbae7afec264bc831641ca6a413b442c5245396641f126e3ae317de45e0a9909 || 0 === strpos($__internal_751e51172e9129657f1f0e84cfe46a6747d912265b8b8dcab59f735f84194238, $__internal_fbae7afec264bc831641ca6a413b442c5245396641f126e3ae317de45e0a9909))))));
        // line 41
        echo "    ";
        if (( !array_key_exists("type", $context) || (isset($context["page_can_upload"]) ? $context["page_can_upload"] : null))) {
            // line 42
            echo "
    ";
            // line 43
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["value"]) ? $context["value"] : null));
            foreach ($context['_seq'] as $context["path"] => $context["file"]) {
                // line 44
                echo "        ";
                echo $this->getAttribute($this, "preview", array(0 => $context["path"], 1 => $context["file"], 2 => $context), "method");
                echo "
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['path'], $context['file'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 46
            echo "
    <div class=\"form-input-wrapper ";
            // line 47
            if ( !($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "fancy", array()) === false)) {
                echo "form-input-file";
            }
            echo " ";
            echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "size", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "size", array()), "xlarge")) : ("xlarge")), "html", null, true);
            echo "\">
        <input
            ";
            // line 50
            echo "            name=\"";
            echo twig_escape_filter($this->env, ($this->env->getExtension('GravTwigExtension')->fieldNameFilter(((isset($context["scope"]) ? $context["scope"] : null) . $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "name", array()))) . (($this->getAttribute((isset($context["files"]) ? $context["files"] : null), "multiple", array())) ? ("[]") : (""))), "html", null, true);
            echo "\"
            ";
            // line 51
            $this->displayBlock('input_attributes', $context, $blocks);
            // line 59
            echo "        />
        ";
            // line 60
            if ( !($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "fancy", array()) === false)) {
                // line 61
                echo "        <p>";
                echo $this->env->getExtension('AdminTwigExtension')->tuFilter("PLUGIN_ADMIN.DROP_FILES_HERE_TO_UPLOAD");
                echo "</p>
        ";
            }
            // line 63
            echo "    </div>

    ";
        } else {
            // line 66
            echo "        <span class=\"note\">";
            echo $this->env->getExtension('AdminTwigExtension')->tuFilter("PLUGIN_ADMIN.CANNOT_ADD_FILES_PAGE_NOT_SAVED");
            echo "</span>
    ";
        }
    }

    // line 51
    public function block_input_attributes($context, array $blocks = array())
    {
        // line 52
        echo "                type=\"file\"
                ";
        // line 53
        if ($this->getAttribute((isset($context["files"]) ? $context["files"] : null), "multiple", array())) {
            echo "multiple=\"multiple\"";
        }
        // line 54
        echo "                ";
        if ($this->getAttribute((isset($context["files"]) ? $context["files"] : null), "accept", array())) {
            echo "accept=\"";
            echo twig_escape_filter($this->env, twig_join_filter($this->getAttribute((isset($context["files"]) ? $context["files"] : null), "accept", array()), ","), "html", null, true);
            echo "\"";
        }
        // line 55
        echo "                ";
        if (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "disabled", array()) || (isset($context["isDisabledToggleable"]) ? $context["isDisabledToggleable"] : null))) {
            echo "disabled=\"disabled\"";
        }
        // line 56
        echo "                ";
        if ($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "random_name", array())) {
            echo "random=\"true\"";
        }
        // line 57
        echo "                ";
        $this->displayParentBlock("input_attributes", $context, $blocks);
        echo "
            ";
    }

    // line 5
    public function getpreview($__path__ = null, $__value__ = null, $__global__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "path" => $__path__,
            "value" => $__value__,
            "global" => $__global__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            // line 6
            echo "    ";
            if ((isset($context["value"]) ? $context["value"] : null)) {
                // line 7
                echo "        ";
                $context["uri"] = $this->getAttribute($this->getAttribute((isset($context["global"]) ? $context["global"] : null), "grav", array()), "uri", array());
                // line 8
                echo "        ";
                $context["files"] = $this->getAttribute((isset($context["global"]) ? $context["global"] : null), "files", array());
                // line 9
                echo "        ";
                $context["config"] = $this->getAttribute($this->getAttribute((isset($context["global"]) ? $context["global"] : null), "grav", array()), "config", array());
                // line 10
                echo "        ";
                $context["route"] = $this->getAttribute($this->getAttribute((isset($context["global"]) ? $context["global"] : null), "context", array()), "route", array(), "method");
                // line 11
                echo "        ";
                $context["type"] = (($this->getAttribute($this->getAttribute((isset($context["global"]) ? $context["global"] : null), "context", array()), "content", array(), "method")) ? ("pages") : ((($this->getAttribute((isset($context["global"]) ? $context["global"] : null), "plugin", array())) ? ("plugins") : ((($this->getAttribute((isset($context["global"]) ? $context["global"] : null), "theme", array())) ? ("themes") : ("config"))))));
                // line 12
                echo "        ";
                $context["blueprint_name"] = $this->getAttribute($this->getAttribute((isset($context["global"]) ? $context["global"] : null), "blueprints", array()), "getFilename", array());
                // line 13
                echo "        ";
                if (((isset($context["type"]) ? $context["type"] : null) == "pages")) {
                    // line 14
                    echo "            ";
                    $context["blueprint_name"] = (((isset($context["type"]) ? $context["type"] : null) . "/") . (isset($context["blueprint_name"]) ? $context["blueprint_name"] : null));
                    // line 15
                    echo "        ";
                }
                // line 16
                echo "        ";
                $context["blueprint"] = base64_encode((isset($context["blueprint_name"]) ? $context["blueprint_name"] : null));
                // line 17
                echo "        ";
                $context["real_path"] = $this->getAttribute($this->getAttribute((isset($context["global"]) ? $context["global"] : null), "admin", array()), "getPagePathFromToken", array(0 => (isset($context["path"]) ? $context["path"] : null)), "method");
                // line 18
                echo "
        ";
                // line 19
                if (((isset($context["type"]) ? $context["type"] : null) == "pages")) {
                    // line 20
                    echo "            ";
                    $this->loadTemplate("forms/fields/hidden/hidden.html.twig", "forms/fields/file/file.html.twig", 20)->display(array_merge($context, array("field" => array("name" => ("data._json." . $this->getAttribute($this->getAttribute((isset($context["global"]) ? $context["global"] : null), "field", array()), "name", array()))), "value" => twig_jsonencode_filter(array($this->getAttribute((isset($context["value"]) ? $context["value"] : null), "path", array()) => (isset($context["value"]) ? $context["value"] : null))))));
                    // line 21
                    echo "        ";
                }
                // line 22
                echo "        <div class=\"file-thumbnail-wrapper\">
            <img class=\"file-thumbnail\" src=\"";
                // line 23
                echo twig_escape_filter($this->env, ((($this->getAttribute((isset($context["uri"]) ? $context["uri"] : null), "rootUrl", array()) == "/")) ? ("/") : (($this->getAttribute((isset($context["uri"]) ? $context["uri"] : null), "rootUrl", array()) . "/"))), "html", null, true);
                echo twig_escape_filter($this->env, (isset($context["real_path"]) ? $context["real_path"] : null), "html", null, true);
                echo "\" alt=\"";
                echo twig_escape_filter($this->env, twig_replace_filter((isset($context["path"]) ? $context["path"] : null), array(($this->getAttribute((isset($context["files"]) ? $context["files"] : null), "destination", array()) . "/") => "")), "html", null, true);
                echo "\" />
            <a class=\"file-thumbnail-remove\" href=\"";
                // line 24
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["uri"]) ? $context["uri"] : null), "addNonce", array(0 => ((((((((((((((((((((((($this->getAttribute((isset($context["global"]) ? $context["global"] : null), "base_url_relative", array()) . "/media/") . base64_encode((($this->getAttribute(                // line 25
(isset($context["global"]) ? $context["global"] : null), "base_path", array()) . "/") . (isset($context["real_path"]) ? $context["real_path"] : null)))) . "/task") . $this->getAttribute($this->getAttribute(                // line 26
(isset($context["config"]) ? $context["config"] : null), "system", array()), "param_sep", array())) . "removeFileFromBlueprint") . "/proute") . $this->getAttribute($this->getAttribute(                // line 27
(isset($context["config"]) ? $context["config"] : null), "system", array()), "param_sep", array())) . base64_encode((isset($context["route"]) ? $context["route"] : null))) . "/blueprint") . $this->getAttribute($this->getAttribute(                // line 28
(isset($context["config"]) ? $context["config"] : null), "system", array()), "param_sep", array())) . (isset($context["blueprint"]) ? $context["blueprint"] : null)) . "/type") . $this->getAttribute($this->getAttribute(                // line 29
(isset($context["config"]) ? $context["config"] : null), "system", array()), "param_sep", array())) . (isset($context["type"]) ? $context["type"] : null)) . "/field") . $this->getAttribute($this->getAttribute(                // line 30
(isset($context["config"]) ? $context["config"] : null), "system", array()), "param_sep", array())) . $this->getAttribute((isset($context["files"]) ? $context["files"] : null), "name", array())) . "/path") . $this->getAttribute($this->getAttribute(                // line 31
(isset($context["config"]) ? $context["config"] : null), "system", array()), "param_sep", array())) . base64_encode($this->getAttribute((isset($context["value"]) ? $context["value"] : null), "path", array()))) . "/redirect") . $this->getAttribute($this->getAttribute(                // line 32
(isset($context["config"]) ? $context["config"] : null), "system", array()), "param_sep", array())) . base64_encode($this->getAttribute((isset($context["uri"]) ? $context["uri"] : null), "path", array()))), 1 => "admin-form", 2 => "admin-nonce"), "method"), "html", null, true);
                echo "\">
                    <i class=\"fa fa-fw fa-close\"></i>
            </a>
        </div>
    ";
            }
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "forms/fields/file/file.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  215 => 32,  214 => 31,  213 => 30,  212 => 29,  211 => 28,  210 => 27,  209 => 26,  208 => 25,  207 => 24,  200 => 23,  197 => 22,  194 => 21,  191 => 20,  189 => 19,  186 => 18,  183 => 17,  180 => 16,  177 => 15,  174 => 14,  171 => 13,  168 => 12,  165 => 11,  162 => 10,  159 => 9,  156 => 8,  153 => 7,  150 => 6,  136 => 5,  129 => 57,  124 => 56,  119 => 55,  112 => 54,  108 => 53,  105 => 52,  102 => 51,  94 => 66,  89 => 63,  83 => 61,  81 => 60,  78 => 59,  76 => 51,  71 => 50,  62 => 47,  59 => 46,  50 => 44,  46 => 43,  43 => 42,  40 => 41,  37 => 40,  34 => 39,  30 => 1,  28 => 3,  26 => 2,  11 => 1,);
    }
}
/* {% extends "forms/field.html.twig" %}*/
/* {% set defaults = config.plugins.form %}*/
/* {% set files = defaults.files|merge(field|default([])) %}*/
/* */
/* {% macro preview(path, value, global) %}*/
/*     {% if value %}*/
/*         {% set uri = global.grav.uri %}*/
/*         {% set files = global.files %}*/
/*         {% set config = global.grav.config %}*/
/*         {% set route = global.context.route() %}*/
/*         {% set type =  global.context.content() ? 'pages' : global.plugin ? 'plugins' : global.theme ? 'themes' : 'config' %}*/
/*         {% set blueprint_name = global.blueprints.getFilename %}*/
/*         {% if type == 'pages' %}*/
/*             {% set blueprint_name = type ~ '/' ~ blueprint_name %}*/
/*         {% endif %}*/
/*         {% set blueprint = base64_encode(blueprint_name) %}*/
/*         {% set real_path = global.admin.getPagePathFromToken(path) %}*/
/* */
/*         {% if type == 'pages' %}*/
/*             {% include 'forms/fields/hidden/hidden.html.twig' with {field: {name: 'data._json.' ~ global.field.name}, value:{(value.path):value}|raw|json_encode} %}*/
/*         {% endif %}*/
/*         <div class="file-thumbnail-wrapper">*/
/*             <img class="file-thumbnail" src="{{ uri.rootUrl == '/' ? '/' : uri.rootUrl ~ '/'}}{{ real_path }}" alt="{{ path|replace({(files.destination ~ '/'): ''}) }}" />*/
/*             <a class="file-thumbnail-remove" href="{{ uri.addNonce(global.base_url_relative ~*/
/*                 '/media/' ~ base64_encode(global.base_path ~ '/' ~ real_path) ~*/
/*                 '/task' ~ config.system.param_sep ~ 'removeFileFromBlueprint' ~*/
/*                 '/proute' ~ config.system.param_sep ~ base64_encode(route) ~*/
/*                 '/blueprint' ~ config.system.param_sep ~ blueprint ~*/
/*                 '/type' ~ config.system.param_sep ~ type ~*/
/*                 '/field' ~ config.system.param_sep ~ files.name ~*/
/*                 '/path' ~ config.system.param_sep ~ base64_encode(value.path) ~*/
/*                 '/redirect' ~ config.system.param_sep ~ base64_encode(uri.path), 'admin-form', 'admin-nonce') }}">*/
/*                     <i class="fa fa-fw fa-close"></i>*/
/*             </a>*/
/*         </div>*/
/*     {% endif %}*/
/* {% endmacro %}*/
/* */
/* {% block input %}*/
/*     {% set page_can_upload = exists or (type == 'page' and not exists and not (field.destination starts with '@self' or field.destination starts with 'self@')) %}*/
/*     {% if type is not defined or page_can_upload %}*/
/* */
/*     {% for path, file in value %}*/
/*         {{ _self.preview(path, file, _context) }}*/
/*     {% endfor %}*/
/* */
/*     <div class="form-input-wrapper {% if field.fancy is not same as(false) %}form-input-file{% endif %} {{ field.size|default('xlarge') }}">*/
/*         <input*/
/*             {# required attribute structures #}*/
/*             name="{{ (scope ~ field.name)|fieldName ~ (files.multiple ? '[]' : '') }}"*/
/*             {% block input_attributes %}*/
/*                 type="file"*/
/*                 {% if files.multiple %}multiple="multiple"{% endif %}*/
/*                 {% if files.accept %}accept="{{ files.accept|join(',') }}"{% endif %}*/
/*                 {% if field.disabled or isDisabledToggleable %}disabled="disabled"{% endif %}*/
/*                 {% if field.random_name %}random="true"{% endif %}*/
/*                 {{ parent() }}*/
/*             {% endblock %}*/
/*         />*/
/*         {% if field.fancy is not same as(false) %}*/
/*         <p>{{ "PLUGIN_ADMIN.DROP_FILES_HERE_TO_UPLOAD"|tu|raw }}</p>*/
/*         {% endif %}*/
/*     </div>*/
/* */
/*     {% else %}*/
/*         <span class="note">{{ "PLUGIN_ADMIN.CANNOT_ADD_FILES_PAGE_NOT_SAVED"|tu|raw }}</span>*/
/*     {% endif %}*/
/* {% endblock %}*/
/* */
