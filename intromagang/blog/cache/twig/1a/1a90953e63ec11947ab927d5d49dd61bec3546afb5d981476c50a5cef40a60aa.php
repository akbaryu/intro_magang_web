<?php

/* partials/nav-toggle.html.twig */
class __TwigTemplate_118e45d14eb3ddf8b8631ebcdbf383a21158aedc7e7bf911b8e478754e4602e2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<button class=\"lines-button x\" type=\"button\" role=\"button\" aria-label=\"Toggle Navigation\" data-sidebar-mobile-toggle>
    <span class=\"lines\"></span>
</button>
";
    }

    public function getTemplateName()
    {
        return "partials/nav-toggle.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <button class="lines-button x" type="button" role="button" aria-label="Toggle Navigation" data-sidebar-mobile-toggle>*/
/*     <span class="lines"></span>*/
/* </button>*/
/* */
