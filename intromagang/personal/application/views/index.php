<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- <link rel="icon" href="favicon.ico"> -->
   <link rel="icon" href="assets/img/ico.png" sizes="64x64" type="image/png">

    <title>Yudha Akbar : Personal Site</title>
    
    <!-- Bootstrap Core CSS -->

    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="assets/css/landing-page.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation">
        <div class="container topnav">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand topnav" href="#"></a>
                <img src="assets/img/logo.png" height="50" alt="">
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#portofolio">Portofolio</a>
                    </li>
                    <li>
                        <a href="#skills">Skills</a>
                    </li>
                    <li>
                        <a href="#blog">Blog</a>
                    </li>
                    <li>
                        <a href="#contact">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>


    <!-- Header -->
    <a name="about"></a>
    <div class="intro-header">
        <div class="container">

            <div class="row">
                <div class="col-lg-12">
                    <div class="intro-message">
                        <h1>Hello !</h1>
                        <h3>I'm Yudha Akbar Pramana</h3>
                        you can call me "Pramono" or "Akbaryu"
                        <hr class="intro-divider">
                        <ul class="list-inline intro-social-buttons">
                           <li>
                            <a href="https://plus.google.com/u/0/+YudhaAkbarPramana/" class="btn btn-default btn-lg"><i class="fa fa-google fa-fw"></i> <span class="network-name">Google</span></a>
                        </li>
                        <li>
                            <a href="https://github.com/akbaryu" class="btn btn-default btn-lg"><i class="fa fa-github fa-fw"></i> <span class="network-name">Github</span></a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-default btn-lg"><i class="fa fa-linkedin fa-fw"></i> <span class="network-name">Linkedin</span></a>
                        </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.intro-header -->

    <!-- Page Content -->

	<a  name="portofolio"></a>
    <div class="content-section-a">

        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">PORTFOLIOS </h2>
                    <p class="lead">This is my portfolio that I've done, you can check at my Git.</p>

                    <a href="https://github.com/akbaryu" class="btn btn-default btn-lg"><i class="fa fa-github fa-fw"></i> <span class="network-name">Github</span></a>
                </div>
                <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                    <img class="img-responsive" src="assets/img/ipad.png" alt="">
                    
                </div>
                
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->
    <a  name="skills"></a>
    <div class="content-section-b">

        <div class="container">

            <div class="row">
                <div class="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">SKILLS</h2>
                    <p class="lead">
                    CODE<br>
                    <h4>HTML, CSS, PHP, Git</h4>
                    <br>
                    DESIGN<br>
                    <h4>Print, Logo, Poster, Cover</h4>
                    <br>
                    DEVELOPMENT
                    <h4>Website, Web Application, Mobile App</h4>
                    <br>
                    TOOLS
                    <h4>PS, AI, AfterEffect, Premiere, Corel, Sublime Text, Github</h4>
                    <br></p>
                </div>
                <div class="col-lg-5 col-sm-pull-6  col-sm-6">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">EXPERIENCES</h2>
                    <p class="lead">
                    2007<br>
                    <h4>SMPN 1 Genteng</h4>
                    <br>
                    2009<br>
                    <h4>SMA 2 Darul Ulum Jombang </h4>
                    <br>
                    2013
                    <h4>Universitas Brawijaya : Teknik Informatika</h4>
                    <br>
                    2016
                    <h4>Magang at Profile Image Studio Malang</h4>
                    <br></p>


                    <br><br><br><br><br><br><br>
                
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-b -->
    <a  name="blog"></a>
    <div class="content-section-a">

        <div class="container">

            <div class="row">
                <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading">BLOG </h2>
                </span>
                <div class="clearfix"></div>
                <ul>
            
                
                <div class="clearfix"></div>
                <div class="btn btn-default btn-lg">
                <div class="col-md-9 fit-left">
                    <a href="../blog" class="btn btn-mini btn-yuri">Read my blog <i class="fa fa-long-arrow-right"></i></a>
                </div>
            </div>

            </ul>
        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->

	<a  name="contact"></a>
    <div class="banner">

        <div class="container">

            <div class="row">
                <div class="col-lg-6">
                    <h2>Ok, now you know me. <br>Let's do business with me ;)</h2>
                </div>
                <div class="col-lg-6">
                    <ul class="list-inline banner-social-buttons">
                        <li>
                            <a href="https://plus.google.com/u/0/+YudhaAkbarPramana/" class="btn btn-default btn-lg"><i class="fa fa-google fa-fw"></i> <span class="network-name">Google</span></a>
                        </li>
                        <li>
                            <a href="https://github.com/akbaryu" class="btn btn-default btn-lg"><i class="fa fa-github fa-fw"></i> <span class="network-name">Github</span></a>
                        </li>
                        <li>
                            <a href="#" class="btn btn-default btn-lg"><i class="fa fa-linkedin fa-fw"></i> <span class="network-name">Linkedin</span></a>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.banner -->

    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="list-inline">
                        <li>
                            <a href="#">Home</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                        <a href="#portofolio">Portofolio</a>
                    </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                        <a href="#skills">Skills</a>
                    </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                        <a href="#blog">Blog</a>
                    </li>
                    <li class="footer-menu-divider">&sdot;</li>
                    <li>
                        <a href="#contact">Contact</a>
                    </li>

                    </ul>
                    <p class="copyright text-muted small">Copyright &copy; FadilHore Team 2016. All Rights Reserved</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
